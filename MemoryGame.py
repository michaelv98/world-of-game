import time, sys, random, os, Utils
from Live import load_game

random_list = []

# generate a list of random numbers between 1 - 101.
# the list length will be difficulty.


def generate_sequence():

    for i in range(0, int(load_game.difficulty)):
        random_list.append(random.randint(0, 101))


    a = list(map(str, random_list))
    a = (" ".join(a))
    generate_sequence.a = a

# return a list of numbers prompted from the user.
# The list length will be in the size of difficulty.


def get_list_from_user(text):
    for i in reversed(range(4)):
        sys.stdout.write('\r')
        sys.stdout.write(text if i % 2 else ''*len(text))
        time.sleep(0.7)
    else:
        os.system('clear\n\n') or os.system("cls\n\n")

    get_list_from_user.user = input("\n\ninsert the numbers : ")

# compare two lists if they are equal.


def is_list_equal():
    while True:
        if generate_sequence.a == get_list_from_user.user:
            print("you won GG")
            break
        elif generate_sequence.a != get_list_from_user.user:
            print(Utils.BAD_RETURN_CODE)
            get_list_from_user.user = input("try again: ")
            return is_list_equal()
        else:
            get_list_from_user.user == ""
            return is_list_equal()

# play the game.


def play():
    generate_sequence()
    get_list_from_user("" + generate_sequence.a)
    is_list_equal()
    Utils.screen_cleaner()



play()