# This file’s sole purpose is to serve the user’s score currently in the scores.txt file over HTTP with HTML.
from flask import Flask, render_template
from pathlib import Path
import os

app = Flask(__name__)


@app.route('/')
def wog_score():
    try:
        score_file = open(Path("Scores.txt"), "r")
        score = score_file.read()
        return render_template('score.html', SCORE=score)#open score 
    except FileNotFoundError or FileExistsError: #if file didnt open 
        return render_template('error.html', ERROR='Unknoiwn Error')

if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, use_reloader=True, host='0.0.0.0', port=port)