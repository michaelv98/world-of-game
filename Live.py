# get the username

def welcome(name):
    print("\n\n"f"Hello {name} and welcome to the World of Games (WoG).\nHere you can find many cool games to play.\n")
    welcome.name = name

# the user choose which game to play

def load_game():
    print("you have three options: \n 1. Guess Game - guess a number and see if you chose like the computer \n 2. Memory Game - a sequence of numbers will appear for 1 second and you have to guess it back.   \n 3. Currency Roulette - try and guess the value of a random amount of USD in ILS")
    level = input("Please choose a game to play: ")
    if level == '1':
      print("You chose the Guess Game ")
        # fun1()
    elif level == '2':
        print("You chose the Memory Game ")
        # fun2()
    elif level == '3':
        print("You chose the Currency Roulette Game" )
        # fun3()
    else:
        print("You must choose between 1 or 2 or 3")
        return load_game()
    print("\nPlease choose game difficulty from 1 to 5")

# the user choose the difficulty

    while True:
        try:
            difficulty = int(input("Enter your choice 1-5: "))
        except ValueError:
            print('That\'s not a number!')
        else:
            if 1 <= difficulty < 6:
                break
            else:
                print('Out of range. Try again')

# print("\nPlease choose game difficulty from 1 to 5")

# import the games

    load_game.difficulty = difficulty
    if level == '1':
        import GuessGame, Score
        from Score import add_score
        add_score()
    if level == '2':
        import MemoryGame
        from Score import add_score
        add_score()