import random, Utils
from Live import load_game

random_list = []

# generate number between 1 to difficulty and save it to secret_number


def generate_number():
    generate_number.secret_number = random.randint(1, int(load_game.difficulty) + 1)



# prompt the user for a number between 1 to difficulty and return the number.


def get_guess_from_user():
    get_guess_from_user.number_of_guesses = 0

    while get_guess_from_user.number_of_guesses < 5:
        try:
            get_guess_from_user.guess = int(input("enter ur guess : "))
        except ValueError:
            print(Utils.BAD_RETURN_CODE)
            print('That\'s not a number!')
        else:
            if get_guess_from_user.guess < generate_number.secret_number:
                print('Your guess is too low')
            if get_guess_from_user.guess > generate_number.secret_number:
                print('Your guess is too high')
            if get_guess_from_user.guess == generate_number.secret_number:
                break
            else:
                print(Utils.BAD_RETURN_CODE)
                print('Out of range. Try again')



# compare the secret generated number to the one prompted by the get_guess_from_user.


def compare_results():
    if get_guess_from_user.guess == generate_number.secret_number:
        print('YOU WON GG')
    else:
        print('You did not guess the number, The number was ' + str(generate_number.secret_number))


# call the functions above and play the game and return True / False if the user lost or won.


def play():
    generate_number()
    get_guess_from_user()
    compare_results()
    Utils.screen_cleaner()

play()